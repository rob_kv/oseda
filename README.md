# OSSAF - Open Source Space Assistant of Flight

Elite: Dangerous Open Source Space Assistant is a VoiceAttack profile created with alot of research behind ease of use and user friendlyness. This Voice activated macro assistant will lift you above the learning curve and almost bypass it.

This is a profile for [VoiceAttack](https://voiceattack.com) (VA) I use 
to enhance my Elite experience. They give me important info

![](media(readme)/img.1.png)

## Documentation

You can find [documentation on GitLab
Pages still WIP]().

## Need Help / Want to Contribute? ##

If you run into any errors, please make sure you are running the latest version
of the profiles and all requirements.

If your problem persists, please [file an
issue](mailto:incoming+rob-kv-oseda-24760547-issue-@incoming.gitlab.com). Thanks! :)

You can also [say “Hi” on Discord](https://discord.gg/jTuA4tw) if that is your 
thing.